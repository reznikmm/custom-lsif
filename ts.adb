with Ada.Text_IO;
procedure TS is
   message : String := "Hello, World!";
begin
   Ada.Text_IO.Put_Line (message);
end;
